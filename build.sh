#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CFLAGS="$CFLAGS -ggdb"
    else
        CFLAGS="$CFLAGS"
    fi

    if [ -z "$OPTIMIZATION" ]; then
        CFLAGS="$CFLAGS -O2"
    else
        CFLAGS="$CFLAGS -O$OPTIMIZATION"
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( cd source
      patch -Np2 -i ../patches/resolve-all-paths-through-this-function-will-call-it.patch
      patch -Np2 -i ../patches/fix_inequality_operator_in_ConstBaseIterator_class.patch)

    ( FREETYPEDIR="${PWD}/../library-freetype/${MULTIARCHNAME}"
      export freetype2_CFLAGS="-I$FREETYPEDIR/include/freetype2"
      export freetype2_LIBS="-L$FREETYPEDIR/lib -lfreetype"
      DEVILDIR="${PWD}/../library-devil/${MULTIARCHNAME}"
      export CFLAGS="$CFLAGS -I$DEVILDIR/include"
      export LDFLAGS="-L$DEVILDIR/lib"
      TIFFDIR="${PWD}/../library-tiff/${MULTIARCHNAME}"
      export LDFLAGS="$LDFLAGS -L$TIFFDIR/lib/ -ltiff"
      PNGDIR="${PWD}/../library-png16/${MULTIARCHNAME}"
      export LDFLAGS="$LDFLAGS -L$PNGDIR/lib/ -lpng"
      JPEGDIR="${PWD}/../library-jpeg-turbo/${MULTIARCHNAME}"
      export LDFLAGS="$LDFLAGS -L$JPEGDIR/lib/ -ljpeg"
      PNGDIR="${PWD}/../library-png16/${MULTIARCHNAME}"
      export CFLAGS="$CFLAGS -I$PNGDIR/include/"
      export LDFLAGS="$LDFLAGS -L$PNGDIR/lib/ -lpng"

      export CFLAGS="$CFLAGS -DTIXML_USE_STL"
      export CXXFLAGS="$CFLAGS"
      cd source
      ./bootstrap
      cd "$BUILDDIR"
      export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$PNGDIR/lib/"
      ../source/configure \
        --prefix="$PREFIXDIR" \
        --disable-static \
        --disable-samples \
        --enable-opengl-renderer \
        --without-xerces-c
      make -j $(nproc)
      make install
      rm -rf "$PREFIXDIR"/{lib/*.la,lib/pkgconfig}
      install -d -m 0755 "$PREFIXDIR"/include/tinyxml
      install -m 0644 ../source/src/tinyxml/tinyxml.h "$PREFIXDIR"/include/tinyxml)

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout . )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="$PWD/build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-cegui04.txt
install -m664 source/PCRE-LICENSE "$PREFIXDIR"/lib/LICENSE-pcre.txt
install -m664 source/TinyXML-License "$PREFIXDIR"/lib/LICENSE-tinyxml.txt

rm -rf "$BUILDDIR"

echo "CEGUI for $MULTIARCHNAME is ready."
