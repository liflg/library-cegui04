Website
=======
http://sourceforge.net/projects/crayzedsgui

License
=======
LGPL v2 (see the file source/COPYING)

Version
=======
0.4.1

Source
======
cegui_mk2-source-0.4.1.tar.bz2 (sha256: a3bdd7f528daa97d2e432994ef249c61b951137aa54aa59dfd04656e3be67d11)

Requires
========
* DevIL
* Freetype
* Jpeg-Turbo
* Tiff

Required by
===========
* Ogre 10
* Ogre 12